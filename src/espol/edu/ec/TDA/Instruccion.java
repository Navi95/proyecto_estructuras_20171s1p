/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.TDA;

/**
 *
 * @author Ivan
 * Clase TDA del objeto Instruccion
 */
public class Instruccion {
    private String operacion;
    private String dirMemoria;
    private int prioridad;

    public Instruccion(String operacion, String dirMemoria, int prioridad) {
        this.operacion = operacion;
        this.dirMemoria = dirMemoria;
        this.prioridad = prioridad;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public String getDirMemoria() {
        return dirMemoria;
    }

    public void setDirMemoria(String dirMemoria) {
        this.dirMemoria = dirMemoria;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }
    
}
