/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.TDA;

import java.util.Stack;

/**
 *
 * @author Ivan
 * El TDA pilaResultado es la representacion de la pila que contendra los valores resultantes de las operaciones indicadas
 * en el archivo instrucciones.txt. Contiene tambien las operaciones especificadas (ADD, SUB, MUL, DIV, PUSH y POP) ademas del metodo toString()
 */
public class PilaResultado { 
    private Stack<Double> pila;

    public PilaResultado() {
        this.pila=new Stack<>();
    }

    public Stack<Double> getPila() {
        return pila;
    }

    public void setPila(Stack<Double> pila) {
        this.pila = pila;
    }
       
    public boolean add()
    {
        if(pila.isEmpty())
        {
           return false; 
        }
        else
        {
            double valor1=pila.pop();
            double valor2=pila.pop();
            pila.push(valor1+valor2);
            return true;
        }
    }
    
    public boolean sub()
    {
        if(pila.isEmpty())
        {
           return false; 
        }
        else
        {
            double valor1=pila.pop();
            double valor2=pila.pop();
            pila.push(valor1-valor2);
            return true;
        }
    }
    
    public boolean mul()
    {
        if(pila.isEmpty())
        {
           return false; 
        }
        else
        {
            double valor1=pila.pop();
            double valor2=pila.pop();
            pila.push(valor1*valor2);
            return true;
        }
    }
    
    public boolean div()
    {
        if(pila.isEmpty())
        {
           return false; 
        }
        else
        {
            double valor1=pila.pop();
            double valor2=pila.pop();
            pila.push(valor1/valor2);
            return true;
        }
    }
    
    public void push(double valor)
    {
        this.pila.push(valor);
    }
    
    public double pop()
    {
        return this.pila.pop();
    }
    @Override
    public String toString()
    {
        String cadena="";
        Stack<Double> temp= (Stack<Double>) this.pila.clone();
        
        while(!temp.isEmpty())
        {
            cadena+=temp.pop()+"\n";
        }
        
        return cadena;
    }
}
