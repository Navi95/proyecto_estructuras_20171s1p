/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import espol.edu.ec.Proyecto.Main;
import espol.edu.ec.Proyecto.Utilidades;
import espol.edu.ec.TDA.Instruccion;
import espol.edu.ec.TDA.Memoria;
import espol.edu.ec.TDA.PilaResultado;
import java.util.Optional;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;
/**
 * FXML Controller class
 *
 * @author Ivan
 * Clase Controlador del view principal del programa
 */
public class ProyectoPilasViewController implements Initializable {

    @FXML
    private TextArea textAreaPila;
    @FXML
    private Label labelInstruccion;
    @FXML
    private Label labelPila;
    @FXML
    private Label labelValInstruccion;
    @FXML
    private Button buttonSiguiente;
    @FXML
    private TextArea textAreaInstrucciones;
    @FXML
    private Label labelInstrucciones;
    @FXML
    private Label labelMemoria;
    @FXML
    private TextArea textAreaMemoria;
    @FXML
    private Button buttonSalir;
    @FXML
    private Button ingresarMemoria;
    @FXML
    private Button borrarMemoria;
    @FXML
    private Button modificarMemoria;
    @FXML
    private Button ingresarInstruccion;
    @FXML
    private Button borrarInstruccion;
    @FXML
    private Button modificarInstruccion;
    
    private Main application;  
    private Stack<Memoria> memoria;
    private PriorityQueue<Instruccion> instrucciones;
    private PilaResultado resultados;

    
    public void setApp(Main application){
        this.application = application;
    }
       
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.resultados= new PilaResultado();
        this.cargar();
        this.setTextMemoriaIntruccionesPila();

    }    
    
    //Llama a los metodos de carga respectivos para el Stack Memoria y el Queue instrucciones
    public void cargar()
    {
        this.memoria=Utilidades.cargarMemoria();
        this.instrucciones=Utilidades.cargarInstrucciones();
    }
    
    //Setea el texto en los textFields pila, memoria e instrucciones
    public void setTextMemoriaIntruccionesPila()
    {
        textAreaInstrucciones.clear();
        textAreaPila.clear();
        textAreaMemoria.clear();
        textAreaInstrucciones.appendText(Utilidades.stringInstrucciones(instrucciones));
        textAreaPila.appendText(this.resultados.toString());
        textAreaMemoria.appendText(Utilidades.stringMemoria(memoria));
    }
    
    //Lee, una por una, las instrucciones en el Queue instrucciones y realiza las operaciones respectivas. Una vez realizadas actualiza el contenido de los textFields
    public void leerInstruccion(ActionEvent event)
    {
       if(!this.instrucciones.isEmpty())
       { 
            Instruccion instruccion=this.instrucciones.poll();
            this.labelValInstruccion.setText(instruccion.getOperacion()+" | "+instruccion.getDirMemoria()+" | "+instruccion.getPrioridad());
            switch(instruccion.getOperacion())
            {
                 case "ADD":
                    if(this.resultados.getPila().size()<2)
                    {
                         Alert alert = new Alert(AlertType.WARNING);
                         alert.setTitle("Error");
                         alert.setHeaderText("Ha ocurrido un error!");
                         alert.setContentText("Verifique que hay por lo menos 2 elementos en la pila\nO que la direccion de memoria sea valida");
                         alert.showAndWait();
                         break;
                    }else
                    {
                        this.resultados.add();
                        break;
                    }

                 case "SUB":
                    if(this.resultados.getPila().size()<2 )
                     {
                          Alert alert = new Alert(AlertType.WARNING);
                          alert.setTitle("Error");
                          alert.setHeaderText("Ha ocurrido un error!");
                          alert.setContentText("Verifique que hay por lo menos 2 elementos en la pila\nO que la direccion de memoria sea valida");
                          alert.showAndWait();
                          break;
                     }else
                     {
                         this.resultados.sub();
                         break;
                     }

                 case "MUL":
                    if(this.resultados.getPila().size()<2)
                     {
                          Alert alert = new Alert(AlertType.WARNING);
                          alert.setTitle("Error");
                          alert.setHeaderText("Ha ocurrido un error!");
                          alert.setContentText("Verifique que hay por lo menos 2 elementos en la pila\nO que la direccion de memoria sea valida");
                          alert.showAndWait();
                          break;
                     }else
                     {
                         this.resultados.mul();
                         break;
                     }

                 case "DIV":
                    if(this.resultados.getPila().size()<2)
                     {
                          Alert alert = new Alert(AlertType.WARNING);
                          alert.setTitle("Error");
                          alert.setHeaderText("Ha ocurrido un error!");
                          alert.setContentText("Verifique que hay por lo menos 2 elementos en la pila\nO que la direccion de memoria sea valida");
                          alert.showAndWait();
                          break;
                     }else
                     {
                         this.resultados.add();
                         break;
                     }

                 case "PUSH":
                    if(!Utilidades.validarDirMemoria(instruccion.getDirMemoria(), memoria))
                    {
                        Alert alert = new Alert(AlertType.WARNING);
                        alert.setTitle("Error");
                        alert.setHeaderText("Ha ocurrido un error!");
                        alert.setContentText("Verifique que la direccion de memoria sea valida");
                        alert.showAndWait();
                        break;
                    }else
                    {
                        double valpush=Utilidades.obtenerValMemoria(instruccion.getDirMemoria(), this.memoria);
                        this.resultados.push(valpush);
                        break;
                    }


                 case "POP":
                    
                    if(this.resultados.getPila().isEmpty())
                    {
                         Alert alert = new Alert(AlertType.WARNING);
                         alert.setTitle("Error");
                         alert.setHeaderText("Ha ocurrido un error!");
                         alert.setContentText("Asegurese que la pila resultante\ntiene almenos 1 elemento para hacer POP");
                         alert.showAndWait();
                         break;
                    }else
                    {
                        double valpop=this.resultados.pop();
                        Utilidades.addMemoria(instruccion.getDirMemoria(), Double.toString(valpop), this.memoria);
                        this.memoria=Utilidades.cargarMemoria();
                        break; 
                    } 
                              
            }
            this.setTextMemoriaIntruccionesPila();
       }    
    }
    
    //ActionHandler del boton Ingresar/Memoria que permite ingresar una direccion de memoria junto a su respectivo valor
    public void ingresarMemoria(ActionEvent event)
    {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Ingresar Memoria");
        dialog.setHeaderText("Ingrese una direccion de memoria");
        dialog.setContentText("(Direccion | valor):");

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            String memoriaStr=result.get();
            String[] memoriaSpt=memoriaStr.split("[|]");
            if(memoriaSpt[0].matches("[A-Z]"))
            {
                if(!Utilidades.validarDirMemoria(memoriaSpt[0], memoria))
                {
                    if(memoriaSpt[1].matches("-?\\d+(\\.\\d+)?"))
                    {
                        Utilidades.addMemoria(memoriaSpt[0],memoriaSpt[1], memoria);
                        this.resultados.getPila().clear();
                        this.cargar();
                        this.setTextMemoriaIntruccionesPila();
                    }
                    else
                    {
                        Alert alert = new Alert(AlertType.INFORMATION);
                        alert.setTitle("Error!");
                        alert.setHeaderText(null);
                        alert.setContentText("Ingrese un valor valido!");
                        alert.showAndWait();
                    }                   
                }else
                {
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Error!");
                    alert.setHeaderText(null);
                    alert.setContentText("La direccion de memoria Ingresada\nYa Existe!");
                    alert.showAndWait();
                }              
            }else
            {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Error!");
                alert.setHeaderText(null);
                alert.setContentText("La direccion no debe ser un numero!\nLa direccion debe estar en mayusculas!");
                alert.showAndWait();             
            }
        }
        this.cargar();
        this.setTextMemoriaIntruccionesPila();
    }
    
    //ActionHandler del boton ingresar/instruccion que permite ingresar una nueva instruccion junto a su respectiva operacion, direccion y prioridad
    public void ingresarInstruccion(ActionEvent event)
    {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Ingresar Instruccion");
        dialog.setHeaderText("Ingrese una Instruccion");
        dialog.setContentText("(Operacion | Direccion | prioridad):");

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            String instruccionStr=result.get();
            String[] instruccionSpt=instruccionStr.split("[|]");
            if("ADD".equals(instruccionSpt[0]) || "SUB".equals(instruccionSpt[0]) || "MUL".equals(instruccionSpt[0]) || "DIV".equals(instruccionSpt[0]) || "PUSH".equals(instruccionSpt[0]) || "POP".equals(instruccionSpt[0]))
            {
                if(instruccionSpt[1].matches("[A-Z]")|| "".equals(instruccionSpt[1]) || " ".equals(instruccionSpt[1]))
                {
                    if(instruccionSpt[2].matches("\\d+")){
                        Utilidades.addInstruccion(instruccionSpt[0], instruccionSpt[1], instruccionSpt[2], instrucciones);
                        this.resultados.getPila().clear();
                        this.cargar();
                        this.setTextMemoriaIntruccionesPila();
                    }
                    else
                    {
                        Alert alert = new Alert(AlertType.INFORMATION);
                        alert.setTitle("Error!");
                        alert.setHeaderText(null);
                        alert.setContentText("La prioridad debe ser un numero entero\nTodo debe estar en mayusculas");
                        alert.showAndWait();        
                    }
                }
                else
                {
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Error!");
                    alert.setHeaderText(null);
                    alert.setContentText("La direccion ingresada no es valida\nTodo debe estar en mayusculas");
                    alert.showAndWait();
                }
            }else
            {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Error!");
                alert.setHeaderText(null);
                alert.setContentText("*La Operacion Ingresada no es valida\n*Todo debe estar en mayusculas");
                alert.showAndWait();
                
            }
        }
        this.cargar();
        this.setTextMemoriaIntruccionesPila();
    }
    
    //ActionHandler del boton borrar/memoria que permite borrar una direccion de memoria indicandola con su respectivo indice
    public void borrarMemoria(ActionEvent event)
    {
        this.resultados.getPila().clear();
        this.labelValInstruccion.setText("");
        this.cargar();
        this.setTextMemoriaIntruccionesPila();
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Borrar Memoria");
        dialog.setHeaderText("Borre una direccion de memoria");
        dialog.setContentText("Ingrese el indice:");

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            String indiceStr=result.get();
            if(indiceStr.matches("\\d+"))
            {
                Utilidades.borrarMemoria(Integer.parseInt(indiceStr), memoria);
                this.resultados.getPila().clear();
                this.cargar();
                this.setTextMemoriaIntruccionesPila();
            }
            else
            {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Error!");
                alert.setHeaderText(null);
                alert.setContentText("*Asegurese que el indice este dentro del rango de memoria\n*El indice debe ser un numero entero positivo");
                alert.showAndWait();
            }
        }   
    }
    
    //ActionHandler del boton borrar/instruccion que permite borrar una instruccion indicandola con su respectivo indice
    public void borrarInstruccion(ActionEvent event)
    {
        this.resultados.getPila().clear();
        this.labelValInstruccion.setText("");
        this.cargar();
        this.setTextMemoriaIntruccionesPila();
        TextInputDialog dialog = new TextInputDialog("Indice");
        dialog.setTitle("Borrar Instruccion");
        dialog.setHeaderText("Borre una instruccion");
        dialog.setContentText("Ingrese el indice:");

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            String indiceStr=result.get();
            if(indiceStr.matches("\\d+"))
            {
                Utilidades.borrarInstruccion(Integer.parseInt(indiceStr),instrucciones);
                this.resultados.getPila().clear();
                this.cargar();
                this.setTextMemoriaIntruccionesPila();
            }
            else
            {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Error!");
                alert.setHeaderText(null);
                alert.setContentText("*Asegurese que el indice este dentro del rango de instrucciones\n*El indice debe ser un numero entero positivo");
                alert.showAndWait();
            }
        } 
        this.cargar();
        this.setTextMemoriaIntruccionesPila();
    }
    
    //ActionHandler del boton modificar/memoria que permite modificar una direccion de memoria que existe en el Stack memoria.
    //Se debe indicar la direccion con su respectivo indice e ingresar la nueva direccion y el nuevo valor.
    public void modificarMemoria(ActionEvent event)
    {
        this.resultados.getPila().clear();
        this.labelValInstruccion.setText("");
        this.cargar();
        this.setTextMemoriaIntruccionesPila();
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("Modificar Memoria");
        dialog.setHeaderText("Modifique una Direccion de memoria");

        ButtonType loginButtonType = new ButtonType("Aceptar", ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField indice = new TextField();
        TextField nuevaMemoria = new TextField();
        nuevaMemoria.setPromptText("Direccion | Valor");

        grid.add(new Label("Indice:"), 0, 0);
        grid.add(indice, 1, 0);
        grid.add(new Label("Memoria:"), 0, 1);
        grid.add(nuevaMemoria, 1, 1);

        dialog.getDialogPane().setContent(grid);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                return new Pair<>(indice.getText(), nuevaMemoria.getText());
            }
            return null;
        });

        Optional<Pair<String, String>> result = dialog.showAndWait();

        if(result.isPresent())
        {
            Pair<String, String> resultPair=result.get();
            String indiceResult=resultPair.getKey();
            String memoriaResult=resultPair.getValue();
            String[] memoriaSpt=memoriaResult.split("[|]");
            if(indiceResult.matches("\\d+")){
                if(memoriaSpt[0].matches("[A-Z]"))
                {
                    if(memoriaSpt[1].matches("-?\\d+"))
                    {
                        Utilidades.modificarMemoria(memoriaSpt[0], memoriaSpt[1], Integer.parseInt(indiceResult), memoria);
                        this.resultados.getPila().clear();
                        this.cargar();
                        this.setTextMemoriaIntruccionesPila();
                    }
                    else
                    {
                        Alert alert = new Alert(AlertType.INFORMATION);
                        alert.setTitle("Error!");
                        alert.setHeaderText(null);
                        alert.setContentText("*Valor ingresado no es valido");
                        alert.showAndWait();
                    }
                }else
                {
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Error!");
                    alert.setHeaderText(null);
                    alert.setContentText("La direccion no debe ser un numero");
                    alert.showAndWait();

                }
            }else
            {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Error!");
                alert.setHeaderText(null);
                alert.setContentText("*Asegurese que el indice este dentro del rango de memoria\n*El indice deber ser un numero entero positivo");
                alert.showAndWait();
                
            }
        }
        this.cargar();
        this.setTextMemoriaIntruccionesPila();
    }
    
    //ActionHandler del boton modificar/instruccion que permite modificar una instruccion que existe en el Queue instrucciones.
    //Se debe indicar la direccion con su respectivo indice e ingresar la respectiva operacion, direccion de memoria y prioridad.
    public void modificarInstruccion()
    {
        this.resultados.getPila().clear();
        this.labelValInstruccion.setText("");
        this.cargar();
        this.setTextMemoriaIntruccionesPila();
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("Modificar Instruccion");
        dialog.setHeaderText("Modifique una Instruccion");

        ButtonType loginButtonType = new ButtonType("Aceptar", ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField indice = new TextField();
        TextField nuevaInstruccion = new TextField();
        nuevaInstruccion.setPrefWidth(200);
        nuevaInstruccion.setPromptText("Operacion | Direccion | Prioridad");

        grid.add(new Label("Indice:"), 0, 0);
        grid.add(indice, 1, 0);
        grid.add(new Label("Instruccion:"), 0, 1);
        grid.add(nuevaInstruccion, 1, 1);

        dialog.getDialogPane().setContent(grid);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                return new Pair<>(indice.getText(), nuevaInstruccion.getText());
            }
            return null;
        });

        Optional<Pair<String, String>> result = dialog.showAndWait();

        if(result.isPresent())
        {
            Pair<String, String> resultPair=result.get();
            String indiceResult=resultPair.getKey();
            String instruccionResult=resultPair.getValue();
            String[] instruccionSpt=instruccionResult.split("[|]");
            if(indiceResult.matches("\\d+"))
            {
                if("ADD".equals(instruccionSpt[0]) || "SUB".equals(instruccionSpt[0]) || "MUL".equals(instruccionSpt[0]) || "DIV".equals(instruccionSpt[0]) || "PUSH".equals(instruccionSpt[0]) || "POP".equals(instruccionSpt[0]))
                {
                    if(instruccionSpt[1].matches("[A-Z]")|| "".equals(instruccionSpt[1]) || " ".equals(instruccionSpt[1]))
                    {
                        if(instruccionSpt[2].matches("\\d+")){
                            Utilidades.modificarInstruccion(instruccionSpt[0], instruccionSpt[1], instruccionSpt[2],Integer.parseInt(indiceResult),instrucciones);
                            this.resultados.getPila().clear();
                            this.cargar();
                            this.setTextMemoriaIntruccionesPila();
                        }
                        else
                        {
                            Alert alert = new Alert(AlertType.INFORMATION);
                            alert.setTitle("Error!");
                            alert.setHeaderText(null);
                            alert.setContentText("*La prioridad debe ser un numero entero\n*Todo debe estar en mayusculas");
                            alert.showAndWait();        
                        }
                    }
                    else
                    {
                        Alert alert = new Alert(AlertType.INFORMATION);
                        alert.setTitle("Error!");
                        alert.setHeaderText(null);
                        alert.setContentText("*La direccion ingresada no es valida\n*Todo debe estar en mayusculas");
                        alert.showAndWait();
                    }
                }else
                {
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Error!");
                    alert.setHeaderText(null);
                    alert.setContentText("*La Operacion Ingresada no es valida\n*Todo debe estar en mayusculas");
                    alert.showAndWait();

                }
            }else
            {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Error!");
                alert.setHeaderText(null);
                alert.setContentText("*Asegurese que el indice este dentro del rango de instrucciones\n*El indice debe ser un numero entero positivo");
                alert.showAndWait();
                
            }
        }
        this.cargar();
        this.setTextMemoriaIntruccionesPila();
    }
}
