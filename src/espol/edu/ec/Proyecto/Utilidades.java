/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.Proyecto;

import espol.edu.ec.TDA.Instruccion;
import espol.edu.ec.TDA.Memoria;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author Ivan
 * Clase que contiene metodos Estaticos que seran utilizados en el programa principal
 */
public class Utilidades {
    
    //Carga el archivo de memoria.txt a un Stack y lo devuelve
    public static Stack<Memoria> cargarMemoria()
    {
        Stack<Memoria> resultado= new Stack<>();
        
        try
        {
            File file=new File("memoria.txt");
            Scanner scanner= new Scanner(file);
            while(scanner.hasNext())
            {
                String linea=scanner.next();
                String[] lineaSplit=linea.split("[|]");
                resultado.push(new Memoria(lineaSplit[0],Double.parseDouble(lineaSplit[1])));
            }
        }
        catch(FileNotFoundException e)
        {
            return resultado;
        }
        
        return resultado;
    }
    
    //Cargar el contenido del archivo instrucciones a una PriorityQueue y lo devuelve
    public static PriorityQueue<Instruccion> cargarInstrucciones()
    {
        PriorityQueue<Instruccion> resultado= new PriorityQueue<>((Instruccion i1, Instruccion i2)->i1.getPrioridad()-i2.getPrioridad());
        
        try
        {
            File file= new File("instrucciones.txt");
            Scanner scanner= new Scanner(file);
            while(scanner.hasNext())
            {
                String linea=scanner.next();
                String[] lineaSplit=linea.split("[|]");
                resultado.offer(new Instruccion(lineaSplit[0],lineaSplit[1],Integer.parseInt(lineaSplit[2])));
            }            
        } 
        catch(FileNotFoundException e)
        {
            return resultado;
        }
        
        return resultado;
    }
    
    //Obtiene el valor de una direccion de memoria en el Stack Memoria
    public static Double obtenerValMemoria(String direccion, Stack<Memoria> memoria)
    {
        if(memoria.isEmpty())
            return null;
        else
        {
            Stack<Memoria> tempStack=(Stack<Memoria>) memoria.clone();
            while(!tempStack.isEmpty())
            {
                Memoria memoriaTemp= tempStack.pop();
                if(direccion.equals(memoriaTemp.getDireccion()))
                {
                    return memoriaTemp.getValor();
                }
            }
        }
        return null;
    }
     
    //Guarda el Stack que contiene los datos de memoria en el archivo memoria.txt (sobreescribe su contenido)
    public static boolean  guardarMemoria(Stack<Memoria> memoria)
    {
        Stack<Memoria> tempPilaMemoria= (Stack<Memoria>) memoria.clone();
        Stack<Memoria> revesMemoria= reversar(tempPilaMemoria);
        
        File file= new File("memoria.txt");
        try (FileWriter memoW = new FileWriter(file, false)) {
                while(!revesMemoria.isEmpty())
                {
                    Memoria tempmemo= revesMemoria.pop();
                    String linea=tempmemo.getDireccion()+"|"+tempmemo.getValor();
                    memoW.write(linea+"\r\n");
                }
            }
        catch(IOException e)
        {
            return false;
        }
        
        return true;    
    }
    
    //Guarda el contenido de el queue instrucciones en el archivo instrucciones.txt (Sobreescribe su contenido)
        public static boolean  guardarInstrucciones(Queue<Instruccion> instrucciones)
    {
        Queue<Instruccion> tempInstrucciones= new LinkedList(instrucciones);

        File file= new File("instrucciones.txt");
        try (FileWriter insW = new FileWriter(file, false)) {
                while(!tempInstrucciones.isEmpty())
                {
                    Instruccion tempInstruccion= tempInstrucciones.poll();
                    String linea=tempInstruccion.getOperacion()+"|"+tempInstruccion.getDirMemoria()+"|"+tempInstruccion.getPrioridad();
                    insW.write(linea+"\n");
                }
            }
        catch(IOException e)
        {
            return false;
        }
        
        return true;
        
    }
     
    //Reversa un Stack y lo devuelve    
    public static Stack reversar(Stack pila){
        Stack reves = new Stack<>();
        Stack temp =   (Stack) pila.clone();
        while(!temp.isEmpty()){
            reves.push(temp.pop());
        }
        return reves;
    }
    
    //Anade un objeto Memoria al Stack de memoria
    public static void addMemoria(String direccion, String valor, Stack<Memoria> memoria)
    {
        if(validarDirMemoria(direccion, memoria))
        {
            Stack<Memoria> tempPilaMemoria= reversar(memoria);
            Stack<Memoria> nuevaMemoria= new Stack<>();
            while(!tempPilaMemoria.isEmpty())
            {
                Memoria tempMemo=tempPilaMemoria.pop();
                if(direccion.equals(tempMemo.getDireccion()))
                {
                    tempMemo.setValor(Double.parseDouble(valor));
                    nuevaMemoria.push(tempMemo);
                }
                nuevaMemoria.push(tempMemo);
            }
            guardarMemoria(memoria);           
        }else
        {
            memoria.push(new Memoria(direccion, Double.parseDouble(valor)));
            guardarMemoria(memoria);           
        }

    }
    
    //Anade un objeto Instruccion al queue de instrucciones
    public static void addInstruccion(String operacion, String dirMemoria, String prioridad, Queue<Instruccion> instrucciones)
    {
        instrucciones.offer(new Instruccion(operacion,dirMemoria, Integer.parseInt(prioridad)));
        guardarInstrucciones(instrucciones);
    }
    
    
    //Borra un elemento del Stack memoria. El elemento es indicado por un indice
    public static void borrarMemoria(int index,Stack<Memoria> memoria )
    {
        Stack<Memoria> revesMemoria= reversar(memoria);
        Stack<Memoria> memoriaBorrada= new Stack<>();
        int idBorrar=0;
        while (!revesMemoria.isEmpty())
        {
            Memoria tempMemoria=revesMemoria.pop();
            if(idBorrar!=index)
            {
                memoriaBorrada.push(tempMemoria);
            }
            idBorrar++;
        }
        guardarMemoria(memoriaBorrada);
    }
    
    //Borrar un elemento del queue instrucciones. El elemento es indicado por un indice
    public static void borrarInstruccion(int index, Queue<Instruccion> instrucciones)
    {
        Queue<Instruccion> tempInstrucciones= new LinkedList(instrucciones);
        Queue<Instruccion> instruccionBorrada= new LinkedList<>();
        int idBorrar=0;
        while(!tempInstrucciones.isEmpty())
        {
            Instruccion tempInstruccion=tempInstrucciones.poll();
            if(idBorrar!=index)
            {
                System.out.println(idBorrar+","+index);
                instruccionBorrada.offer(tempInstruccion);
            }
            idBorrar++;
        }
        guardarInstrucciones(instruccionBorrada);
        
    }
    
    //Modifica los atributos de un objeto memoria contenido en el Stack memoria. El elemento es indicado por un indice
    public static void modificarMemoria(String direccion, String valor, int index, Stack<Memoria> memoria)
    {
        Stack<Memoria> tempPilaMemoria= reversar(memoria);
        Stack<Memoria> memoriaBorrada= new Stack<>();
        int idBorrar=0;
        while (!tempPilaMemoria.isEmpty())
        {
            if(idBorrar==((memoria.size()-1)-index))
            {
                tempPilaMemoria.pop();
                memoriaBorrada.push(new Memoria(direccion, Double.parseDouble(valor)));
            }
            else
            {
                memoriaBorrada.push(tempPilaMemoria.pop());
            }
            idBorrar++;
        }
        guardarMemoria(memoriaBorrada);
    }
    
    //Modifica los atributos de un objeto instruccion contenido en el Queue instrucciones. El elemento es indicado por un indice
    public static void modificarInstruccion(String operacion, String dirMemoria, String prioridad, int index, Queue<Instruccion> instrucciones )
    {
        Queue<Instruccion> tempInstrucciones= new LinkedList(instrucciones);
        Queue<Instruccion> instruccionBorrada= new LinkedList<>();
        Queue<Instruccion> instruccionesOriginales= cargarInstrucciones();
        int idBorrar=0;
        while(!tempInstrucciones.isEmpty())
        {
            if(idBorrar==index)
            {
                tempInstrucciones.poll();
                instruccionBorrada.offer(new Instruccion(operacion, dirMemoria, Integer.parseInt(prioridad)));
            }else
            {
                instruccionBorrada.offer(tempInstrucciones.poll());                
            }
            idBorrar++;
        }
        guardarInstrucciones(instruccionBorrada);
    }
    
    //Metodo toString para el queue de instrucciones
    public static String stringInstrucciones(Queue<Instruccion> instrucciones)
    {
        PriorityQueue<Instruccion> tempIntrucciones=new PriorityQueue(instrucciones);
        String cadena="";
        int index=0;
        while(!tempIntrucciones.isEmpty())
        {
            Instruccion tempInstruccion=tempIntrucciones.poll();
            cadena+=index+" -> "+tempInstruccion.getOperacion()+" | "+tempInstruccion.getDirMemoria()+" | "+tempInstruccion.getPrioridad()+" \n";
            index++;
        }
        return cadena;
    }
    
    //Metodo toString para el Stack de memoria
    public static String stringMemoria(Stack<Memoria> memoria)
    {
        Stack<Memoria> tempMemoria= (Stack<Memoria>) memoria.clone();
        Stack<Memoria> revesMemoria=reversar(tempMemoria);
        String cadena="";
        int index=0;
        while(!revesMemoria.isEmpty())
        {
            Memoria tempMemo=revesMemoria.pop();
            cadena+=index+" -> "+tempMemo.getDireccion()+" | "+tempMemo.getValor()+" \n";
            index++;
        }
        
        return cadena;
    }
    
    //valida si una direccion de memoria se encuentra en el Stack memoria. Retorna true si el elemento se encuentra en el Stack
    public static boolean validarDirMemoria(String direccion, Stack<Memoria> memoria)
    {
        Stack<Memoria> tempMemorias=(Stack<Memoria>) memoria.clone();
        
        while(!tempMemorias.isEmpty())
        {
            if(tempMemorias.pop().getDireccion().equals(direccion))
                return true;
        }
        
        return false;
    }
 
}
